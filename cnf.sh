#! /bin/bash

# to run: wget -O - https://gitlab.com/etlawby/udf-cne/-/raw/main/cnf.sh | bash
# run after deploying K8s with https://gitlab.com/etlawby/udf-cne/-/raw/main/k8s.sh

wget -xnH -O - repo.secure-demo.net/cnf-xc-boot | bash
wget -N repo.secure-demo.net/config/cnf/v1.0.5/nginx.yaml
wget -xnH -O - repo.secure-demo.net/repo-boot | bash