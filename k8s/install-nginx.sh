#! /bin/bash

kubectl label node node1 type=f5-control
kubectl label node node2 type=worker
kubectl label node node3 type=worker
kubectl label node node4 type=dpu
kubectl label node node5 type=dpu
kubectl label node node4 app=f5-tmm
kubectl label node node5 app=f5-tmm

kubectl create ns red
kubectl create ns blue
kubectl apply -f nginx-svc-red-blue.yaml 
kubectl apply -f nginx-generic.yaml -n blue
kubectl apply -f nginx-generic.yaml -n red

