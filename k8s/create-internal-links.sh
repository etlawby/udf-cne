#! /bin/bash

# create-internal-links.sh
# updates netplan config to create GRE tunnel between DPU and Server to represent PCIE connection

echo "setting up internal connections between DPUs and Servers"

# node4 (dpu1) -- pcie -- node2 (server1)
ssh ubuntu@node2 'sudo tee /etc/netplan/70-netplan-set.yaml > /dev/null' <<EOF
network:
  version: 2
  ethernets:
    ens6:
      dhcp4: true
  tunnels:
      pcie:
        mode: gretap
        remote: 10.1.110.11
        local: 10.1.110.13
        ttl: 255
        addresses:
          - 10.1.20.13/24
        dhcp4: no
EOF

ssh ubuntu@node4 'sudo tee /etc/netplan/70-netplan-set.yaml > /dev/null' <<EOF
network:
  version: 2
  renderer: networkd
  ethernets:
    ens6:
      dhcp4: true
    ens7:
      dhcp4: false
    ens8:
      dhcp4: true
  bridges:
    br0:
      interfaces:
        - ens7
        - pcie
      addresses:
        - 10.1.20.11/24
      dhcp4: no
      optional: true
  tunnels:
    pcie:
      mode: gretap
      remote: 10.1.110.13
      local: 10.1.110.11
      ttl: 255
EOF

# node5 (dpu2) -- pcie -- node3 (server2)
ssh ubuntu@node3 'sudo tee /etc/netplan/70-netplan-set.yaml > /dev/null' <<EOF
network:
  version: 2
  ethernets:
    ens6:
      dhcp4: true
  tunnels:
      pcie:
        mode: gretap
        remote: 10.1.120.12
        local: 10.1.120.14
        ttl: 255
        addresses:
          - 10.1.20.14/24
        dhcp4: no
EOF

ssh ubuntu@node5 'sudo tee /etc/netplan/70-netplan-set.yaml > /dev/null' <<EOF
network:
  version: 2
  renderer: networkd
  ethernets:
    ens6:
      dhcp4: true
    ens7:
      dhcp4: false
    ens8:
      dhcp4: true
  bridges:
    br0:
      interfaces:
        - ens7
        - pcie
      addresses:
        - 10.1.20.12/24
      dhcp4: no
      optional: true
  tunnels:
    pcie:
      mode: gretap
      remote: 10.1.120.14
      local: 10.1.120.12
      ttl: 255
EOF

ssh ubuntu@node2 'sudo netplan apply'
ssh ubuntu@node3 'sudo netplan apply'
ssh ubuntu@node4 'sudo netplan apply'
ssh ubuntu@node5 'sudo netplan apply'

# change CNI to use internal-net for dpus/servers
calicoctl --allow-version-mismatch patch node node2 -p '{"spec": {"bgp": {"ipv4Address": "10.1.20.13"}}}' --type=merge     
calicoctl --allow-version-mismatch patch node node3 -p '{"spec": {"bgp": {"ipv4Address": "10.1.20.14"}}}' --type=merge
calicoctl --allow-version-mismatch patch node node4 -p '{"spec": {"bgp": {"ipv4Address": "10.1.20.11"}}}' --type=merge
calicoctl --allow-version-mismatch patch node node5 -p '{"spec": {"bgp": {"ipv4Address": "10.1.20.12"}}}' --type=merge
