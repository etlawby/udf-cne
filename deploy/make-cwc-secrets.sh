#! /bin/bash

# set-up environment before starting
function tar () { /usr/bin/tar $@ > /dev/null; }
version="spk/v1.5.0"
#pushd ~/software/$version >> /dev/null


# SPK CWC/Procedures/Create cluster Secrets and CWC certificates
#cd spkinstall
#ls -1 tar
#tar xvf tar/f5-cert-gen-0.2.4.tgz
mkdir -p cert-gen
wget https://gitlab.com/etlawby/udf-cne/-/raw/main/deploy/gen_cert.sh 
sh cert-gen/gen_cert.sh -s=api-server -a=f5-spk-cwc.spk-telemetry -n=1 > /dev/null 2>&1 
#kubectl apply -f cwc-license-certs.yaml -n spk-telemetry

sh cert-gen/gen_cert.sh -s=rabbit -a=rabbitmq-server.spk-telemetry.svc.cluster.local -n=3 > /dev/null 2>&1
echo "  ### generated: $(date)" >> rabbitmq-client-certs.yaml
echo "  ### generated: $(date)" >> rabbitmq-server-certs.yaml

#kubectl apply -f rabbitmq-client-certs.yaml -n spk-telemetry
#kubectl apply -f rabbitmq-server-certs.yaml -n spk-telemetry

# Install the CPCL certifcate and key
# cpcl-cert and cpcl-key from F5 licencing
echo "apiVersion: v1" > cpcl-cert.yaml
echo "kind: ConfigMap" >> cpcl-cert.yaml
echo "metadata:" >> cpcl-cert.yaml
echo "  name: cpcl-crt-cm" >> cpcl-cert.yaml
echo "data:" >> cpcl-cert.yaml
echo "  jwt_ca.crt: |+" >> cpcl-cert.yaml
cat cpcl-cert-data >> cpcl-cert.yaml
#kubectl apply -f cpcl-cert.yaml -n spk-telemetry

#curl -q https://product.apis.f5.com/ee/v1/keys/jwks > cpcl-key-data
echo "apiVersion: v1" > cpcl-key.yaml
echo "kind: ConfigMap" >> cpcl-key.yaml
echo "metadata:" >> cpcl-key.yaml
echo "  name: cpcl-key-cm" >> cpcl-key.yaml
echo "data:" >> cpcl-key.yaml
echo "  jwt.key: |+" >> cpcl-key.yaml
cat cpcl-key-data >> cpcl-key.yaml
#kubectl apply -f cpcl-key.yaml -n spk-telemetry

cd ..
cd spkinstall
#ls -1 tar

echo "image:" > cwc-values.yaml
echo "  repository: gitlab.f5.local/root/my-spk" >> cwc-values.yaml
#helm install spk-cwc tar/cwc-0.4.15.tgz -f cwc-values.yaml --set cpclConfig.jwt=$f5jwt -n spk-telemetry
cp tar/cwc-0.4.15.tgz ~/my-spk/cwc

popd >> /dev/null
