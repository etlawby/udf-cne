#! /bin/bash
### ~/my-repo/scripts/download.sh ###

DIR="/home/ubuntu/my-repo/"
MANIFEST="manifest.txt"
REPO="https://repo.secure-demo.net"
CMD="wget --force-directories --no-host-directories --timestamping ${REPO}/"

if [ -z "$1" ]; then
  filter=""
  echo "Downloading all"
else
  filter="$1"
  echo "Download ${filter}"
fi

mkdir -p ${DIR}
pushd ${DIR} >> /dev/null

curl -s ${REPO}/${MANIFEST} > ${MANIFEST}

echo "manifest done"
sleep 5

while read -r line; do 
  if [ -z "$(echo ${line} | grep "^#")" ]; then
    if [ ! -d $(echo ${line} | grep "^${filter}") ]; then
      if [ -d $(echo ${line} | sed "s|^\([^/]*\)/\([^/]*\).*|\1/\2/\1install|") ]; then
        echo "skipping ${line} - already downloaded"
      else
#        echo "fetching ${line}..."
        response=$(${CMD}${line} 2>&1 | grep '^HTTP request sent, awaiting response...' | sed "s|^.*\.\.\. ||")
        echo "${response} ${line}"
      fi
#    else
#      echo "filtered ${line}"
    fi
  else
    # print comment
	echo "${line}"
  fi
done < ${MANIFEST}

