#! /bin/bash

### full deploy of Kubernetes

# to run:
# TEST=0 bash -c "$(wget -O - https://gitlab.com/etlawby/udf-cne/-/raw/main/k8s.sh)"

# blueprint: CNF-base https://udf.f5.com/b/3f0450b2-509d-4844-820b-72222208ea35
# deploy on version v3
# run on Jumphost component

PROJECT="udf-cne"
TEST=${TEST:-1}  # Set TEST to 1 if not already set

echo "*** LOG STARTED ***" | sudo tee -a /var/log/${PROJECT}.log > /dev/null 
cd
rm -rf ~/*
sudo apt update
sudo NEEDRESTART_MODE=a apt install git -y

# fd874bf1
git clone https://gitlab.com/etlawby/${PROJECT}
ln -s ~/udf-cne/install/library.sh ~/${PROJECT}/uninstall/library.sh 
#git reset --hard 9d273718

chmod 755  ~/${PROJECT}/install/*
chmod 755  ~/${PROJECT}/uninstall/*

if [ "${TEST}" == "1" ]; then
  run-parts ~/${PROJECT}/install --regex '^[0-9][0-9]-.*.sh' --verbose --test
  run-parts ~/${PROJECT}/install --regex '^[1-9][0-9][0-9]-.*.sh' --verbose --test
  echo "*** Dryrun mode ***"
  echo "usage: TEST=0 bash -c \"\$(wget -O - https://gitlab.com/etlawby/udf-cne/-/raw/main/k8s.sh)\""
else
  run-parts ~/${PROJECT}/install --regex '^[0-9][0-9]-.*.sh' --verbose --exit-on-error
  run-parts ~/${PROJECT}/install --regex '^[1-9][0-9][0-9]-.*.sh' --verbose --exit-on-error
fi

