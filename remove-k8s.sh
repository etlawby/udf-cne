#! /bin/bash

### full removal of Kubernetes

# to run:
# TEST=0 bash -c "$(wget -O - https://gitlab.com/etlawby/udf-cne/-/raw/main/remove-k8s.sh)"
# assumes deployment using https://repo.secure-demo.net/udf/k8s.sh

TEST=${TEST:-1}  # Set TEST to 1 if not already set

cd

if [ "${TEST}" == "1" ]; then
  run-parts ~/udf-cne/uninstall --reverse --regex '[1-9][0-9][0-9]-.*.sh' --verbose --test
  run-parts ~/udf-cne/uninstall --reverse --regex '[0-9][0-9]-.*.sh' --verbose --test
  echo "*** Dryrun mode ***"
  echo "usage: TEST=0 bash -c \"\$(wget -O - https://gitlab.com/etlawby/udf-cne/-/raw/main/remove-k8s.sh)\""
else
  run-parts ~/udf-cne/uninstall --reverse --regex '[1-9][0-9][0-9]-.*.sh' --verbose
  run-parts ~/udf-cne/uninstall --reverse --regex '[0-9][0-9]-.*.sh' --verbose
  rm -rf ~/*
fi
