#! /bin/bash

main () {
  configure_ssh
}

configure_ssh () {

require sshpass
require_file ~/.ssh/id_rsa.pub

export PASSWORD="HelloUDF"
add_key ubuntu@client
add_key ubuntu@server

total_nodes="$(component_count node)"
for ((i = 1; i <= ${total_nodes}; i++)); do
  add_key ubuntu@node${i}
done


#export PASSWORD="default"
#check_ssh root@dp1
#check_ssh root@dp2
#check_ssh root@cp1

# do not use ssh-copy-id with VYOS
# VYOS SSH access configured in router config

}

add_key () {
  ssh -q -o PreferredAuthentications=publickey -o StrictHostKeyChecking=accept-new ${1} exit
  echo "${PASSWORD}" | sshpass ssh-copy-id ${1}
  ssh -q -o PreferredAuthentications=publickey ${1} exit
  if [ ${?} -eq 0 ]; then
    echo "SSH public key authentication to $1 OK"
  else
    echo "add public key ito $1"
    echo "echo \"$(cat ~/.ssh/id_rsa.pub)\" >> ~/.ssh/authorized_keys" 
  fi
}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
