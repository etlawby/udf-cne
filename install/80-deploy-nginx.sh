#! /bin/bash

LOCAL_DOMAIN="udf"

main () {
  deploy_nginx
}

### configure nginx ###

deploy_nginx () {


SUBNET_INT="10.1.30.0/24"
SUBNET_EXT="10.1.40.0/24"

CREDS="ubuntu@client"
ssh -q -o PreferredAuthentications=publickey ${CREDS} exit
if [ ${?} -eq 0 ]; then
  ssh "${CREDS}" "sudo hostnamectl set-hostname client"
  add_interface "${CREDS}" "ens6"
  set_dns "${CREDS}" "$(get_component_ip jumphost)"
  install_nginx "${CREDS}"
  set_default_route "${CREDS}" "$(get_interface "access router" "${SUBNET_INT}")"
else
  log warning "${CREDS} Public Key not available"
fi

CREDS="ubuntu@server"
ssh -q -o PreferredAuthentications=publickey ${CREDS} exit
if [ ${?} -eq 0 ]; then
  ssh "${CREDS}" "sudo hostnamectl set-hostname server"
  add_interface "${CREDS}" "ens6"
  add_interface "${CREDS}" "ens7"
  set_dns "${CREDS}" "$(get_component_ip jumphost)"
  install_nginx "${CREDS}"
  set_default_route "${CREDS}" "$(get_interface "internet router" "${SUBNET_EXT}")"
else
  log warning "${CREDS} Public Key not available"
fi

}

install_nginx () {
  CREDS="${1}"
  NGINX_CONF="/etc/nginx/sites-available/my-nginx.conf"
  ssh "${CREDS}" <<EOF
sudo apt update
sudo NEEDRESTART_SUSPEND=1 apt -y install nginx fcgiwrap
sudo rm /etc/nginx/sites-enabled/*
cat <<"EOF2" | sudo tee ${NGINX_CONF} > /dev/null
### Nginx configuration
server {
  listen 80;
  location / {
    default_type text/plain;
    return 200 "Server \$server_name\n\$remote_addr:\$remote_port-->\$server_addr:\$server_port\n";
  }
}
EOF2
sudo ln -s  ${NGINX_CONF} /etc/nginx/sites-enabled/
sudo systemctl reload nginx
EOF
}

add_interface () {
  CREDS="${1}"
  IF="${2}"
  ssh "${CREDS}" sudo netplan set ethernets."${IF}".dhcp4=true
  ssh "${CREDS}" sudo netplan apply  
}

set_default_route () {
  CREDS="${1}"
  ROUTE="${2}"
  ssh "${CREDS}" > /dev/null 2>&1 <<EOF
sudo netplan set ethernets.ens5.dhcp4=true
sudo netplan set ethernets.ens5.dhcp4-overrides.use-routes=false
sudo netplan set ethernets.ens5.routes='[{"to":"0.0.0.0/0", "via": "'"${ROUTE}"'","metric":"0"}]'
if ip link show "ens6" &>/dev/null; then sudo netplan set ethernets.ens6.dhcp4=true; fi
if ip link show "ens7" &>/dev/null; then sudo netplan set ethernets.ens7.dhcp4=true; fi
if ip link show "ens8" &>/dev/null; then sudo netplan set ethernets.ens8.dhcp4=true; fi
sudo netplan apply
EOF
}

set_dns () {
  CREDS="${1}"
  DNS="${2}"
  ssh "${CREDS}" > /dev/null 2>&1 <<EOF
sudo netplan set ethernets.ens5.dhcp4=true
sudo netplan set ethernets.ens5.dhcp4-overrides.use-dns=false
sudo netplan set ethernets.ens5.nameservers.addresses='["'"${DNS}"'"]'
sudo netplan apply
sudo sed -i "s|^#*DNS=.*$|DNS="${DNS}"|" /etc/systemd/resolved.conf
sudo sed -i "s|^#*Domains=.*$|Domains=${LOCAL_DOMAIN}|" /etc/systemd/resolved.conf
sudo systemctl restart systemd-resolved
EOF
test_dns "${CREDS}"
}

# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit

