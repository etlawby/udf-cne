#! /bin/bash

main () {
  configure_dns
}

configure_dns () {

# clean up any existing entries
#sudo sed -i "s|^127.0.0.1 .*$|127.0.0.1 localhost localhost.localdomain|" /etc/hosts
sudo sed -i "s|^127.0.0.1 .*$|127.0.0.1 localhost $(hostname)|" /etc/hosts
sudo sed -i "/^###/q" /etc/hosts
sudo sed -i "/^###/d" /etc/hosts
sudo sed -i "/^10\./d" /etc/hosts

cat << EOF | sudo tee -a /etc/hosts > /dev/null
###############################################################
### additional authoratative DNS records for UDF deployment ###
###############################################################
10.1.1.1 metadata.udf #required first
EOF

# add all components automatically
curl -Ls http://metadata.udf/deployment | jq '.deployment.components[] | "\(.mgmtIp) \(.name)"' | cut -d'"' -f2 | sudo tee -a /etc/hosts > /dev/null

# customise component enties
total_nodes="$(curl -s metadata.udf/deployment | jq '.deployment.components[] | "\(.name)"' -c | tr -d '"' | grep "^k8s-\|^node" | wc -l)"
for ((i = 1; i <= ${total_nodes}; i++)); do
  update_hosts_entry "$(get_component_ip_exists node${i})" k8s-${i} node${i}
  update_hosts_entry "$(get_component_ip_exists k8s-${i})" k8s-${i} node${i}
done

update_hosts_entry "$(get_component_ip_exists 'jumphost')" jumphost
update_hosts_entry "$(get_component_ip_exists 'server1')" server1 server
update_hosts_entry "$(get_component_ip_exists 'client1')" client1 client
update_hosts_entry "$(get_component_ip_exists 'client2')" client2

update_hosts_entry "$(get_component_ip_exists 'internet-router')" router2 r2 internet-router
update_hosts_entry "$(get_component_ip_exists 'access-router')" router1 r1 access-router
update_hosts_entry "$(get_component_ip_exists 'analytics')" analytics

update_hosts_entry "$(get_component_ip_exists 'dp1')" dp1 data
update_hosts_entry "$(get_component_ip_exists 'dp2')" dp2
update_hosts_entry "$(get_component_ip_exists 'cp1')" cp1 control
update_hosts_entry "$(get_component_ip_exists 'bigiq')" bigiq biq

update_hosts_entry "10.1.20.31" web1.cnf.local
update_hosts_entry "10.200.121.1" amf.cnf.local app1.cnf.local
update_hosts_entry "10.200.131.1" smf.cnf.local app2.cnf.local

# compabability entries
update_hosts_entry "$(get_component_ip_exists 'server')" server server1
update_hosts_entry "$(get_component_ip_exists 'client')" client client1
update_hosts_entry "$(get_component_ip_exists 'ecmp router')" ecmp-router
update_hosts_entry "$(get_component_ip_exists 'internet router')" router2 r2 internet-router
update_hosts_entry "$(get_component_ip_exists 'access router')" router1 r1 access-router

# BNK entries
update_hosts_entry "$(get_component_ip_exists 'control-plane')" k8s-1 node1 control-plane
update_hosts_entry "$(get_component_ip_exists 'server1')" k8s-2 node2 server1
update_hosts_entry "$(get_component_ip_exists 'server2')" k8s-3 node3 server2
update_hosts_entry "$(get_component_ip_exists 'dpu1')" k8s-4 node4 dpu1
update_hosts_entry "$(get_component_ip_exists 'dpu2')" k8s-5 node5 dpu2

# additional DNS entries
cat << EOF | sudo tee -a /etc/hosts > /dev/null
10.1.200.1 web.cnf.local
10.200.121.1 amf.cnf.local app1.cnf.local
10.200.131.1 smf.cnf.local app2.cnf.local
3.122.124.33 wireguard.secure-demo.net
10.1.1.1 metadata.udf
EOF

# configure jumphost as DNS server
# disabled - using DNSMASQ instead of resolved
#ext_ip="$(ip addr show dev ens5 | grep "inet " | sed "s|.*inet \(.*\)/.*$|\1|")"
#sudo sed -i "s|^#*DNSStubListenerExtra=.*$|DNSStubListenerExtra=${ext_ip}|" /etc/systemd/resolved.conf
#sudo systemctl restart systemd-resolved

sudo systemctl restart dnsmasq

}

update_hosts_entry () {
  entry="${*}"
  ip="${1}"
  # only add entry 
  if [ "${ip}" != "0" ]; then
    sudo sed -i "/^${ip}/d" /etc/hosts
    echo "${entry}" | sudo tee -a /etc/hosts > /dev/null
    echo "Added ${entry} to /etc/hosts"
  fi
}

get_component_ip_exists () {
  ip="$(get_component_ip "${1}")"
  if [ -z "${ip}" ]; then
    echo "0"
  else
    echo "${ip}"
  fi
}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
