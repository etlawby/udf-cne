#! /bin/bash

# wget -O - https://gitlab.com/etlawby/udf-cne/-/raw/main/k8s.sh | bash
# tail -f /var/log/udf-cne.log 

HOSTNAME="jumphost"
IP="$(ip addr | grep "10\.1\.1\." | sed "s|^.*inet \([^\/]*\).*$|\1|")"
DNS_CONF="/etc/dnsmasq.conf"
LOCAL_DOMAIN="udf"

main () {
  configure_jumphost
}

### configure jumphost ###

configure_jumphost () {

sudo unlink /etc/resolv.conf
echo "nameserver 10.1.1.2" | sudo tee /etc/resolv.conf > /dev/null
sudo systemctl disable systemd-resolved
sudo systemctl stop systemd-resolved
#sudo sed -i "s|^127.0.0.1 .*$|127.0.0.1 localhost ${HOSTNAME}|" /etc/hosts
sudo sed -i "s|^127.0.0.1 .*$|127.0.0.1 localhost|" /etc/hosts
echo "${IP} ${HOSTNAME}" | sudo tee -a /etc/hosts > /dev/null
sudo hostnamectl hostname "${HOSTNAME}"

sudo apt update
sudo NEEDRESTART_MODE=a apt install snap sshpass git ansible python3 python3-pip python3-virtualenv dnsmasq dnsutils -y

cat << EOF | sudo tee ${DNS_CONF} > /dev/null
expand-hosts
domain-needed
domain=${LOCAL_DOMAIN}
local=/${LOCAL_DOMAIN}/
localise-queries
no-resolv
server=10.1.1.2
bind-interfaces
except-interface=nonexisting
no-dhcp-interface=lo
cache-size=10000
bogus-priv
EOF

# temporary entry for metadata.udf - required
echo "10.1.1.1 metadata.udf" | sudo tee -a /etc/hosts > /dev/null
echo "nameserver 127.0.0.1" | sudo tee /etc/resolv.conf > /dev/null
sudo systemctl restart dnsmasq

sudo snap install kubectl --classic
sudo snap install helm --classic
sudo snap install jq
sudo snap install jo
sudo snap install yq
pip3 install ansible==9.13.0
pip3 install netaddr


if [ ! -e ~/.ssh/id_rsa.pub ]; then
  ssh-keygen -q -t rsa -N "" -f ~/.ssh/id_rsa
fi
rm ~/.ssh/known_hosts

curl -sS https://webinstall.dev/k9s | bash
source ~/.config/envman/PATH.env
 
(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
  tar zxvf "${KREW}.tar.gz" &&
  ./"${KREW}" install krew
)

curl -OLs https://github.com/projectcalico/calico/releases/latest/download/calicoctl-linux-amd64
chmod +x calicoctl-linux-amd64
sudo mv calicoctl-linux-amd64 /usr/local/bin/calicoctl

echo 'export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"' >> ~/.bashrc 
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

cat > ~/.bash_aliases << "EOF"
source <(kubectl completion bash)
alias k="kubectl"
alias kc="kubectl"
complete -o default -F __start_kubectl k
complete -o default -F __start_kubectl kc
alias calicoctl="calicoctl --allow-version-mismatch"
alias r1="ssh vyos@router1"
alias r2="ssh vyos@router2"
alias client="ssh ubuntu@client"
alias server="ssh ubuntu@server"
alias vi="vim -C --noplugin -c 'syn off'"
alias history="history | sed 's|^[[:space:]]*[0-9]\+[[:space:]]*||'"
alias ns='kubectl config set-context --current --namespace'
alias crds="kubectl get crds | grep '^f5' | sed 's| .*$||'"

alias tmm="kubectl exec -it deployment/f5-tmm -c debug -- tput rmam; kubectl exec -it deployment/f5-tmm -c debug --"
alias tmmdump="kubectl exec -it deployment/f5-tmm -c debug -- tput rmam; kubectl exec -it deployment/f5-tmm -c debug -- tcpdump -nnni 0.0:nnn"
alias tmstats="kubectl exec -it deployment/f5-tmm -c debug -n f5-cnf -- tmctl -f /var/tmstat/blade/tmm0 virtual_server_stat -s name,serverside.tot_conns"
#alias configview="$kubectl exec -it `kubectl get pod -l app=f5-tmm | grep f5-tmm | awk '{print $1}'` -c debug -- configviewer --ipport=tmm0:11211 --displayall"
alias scale="kubectl scale deploy/f5-tmm --replicas"
alias nuke="wget -O - https://gitlab.com/etlawby/udf-cne/-/raw/main/remove-k8s.sh | bash"
alias pave="wget -O - https://gitlab.com/etlawby/udf-cne/-/raw/main/k8s.sh | bash"

EOF



# automatically switch to ubuntu user if we arrive from UDF webshell
echo "su - ubuntu" | sudo tee /root/.bash_aliases > /dev/null

source ~/.bashrc # not applied
kubectl krew install virt

}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
