#! /bin/bash

MAX_CONTROL="1"
MAX_ETCD="3"

main () {
  generate_inventory
}

generate_inventory () {

total_nodes="$(component_count node)"
echo "Total Nodes: ${total_nodes}"

if [  "${total_nodes}" == "0" ]; then
  echo "FATAL DNS ERROR - aborting"
  exit 1
fi

cd ~/.
rm -rf kubespray
git clone --depth=1 https://github.com/kubernetes-sigs/kubespray.git
KUBESPRAYDIR=$(pwd)/kubespray/
MYCLUSTER="${KUBESPRAYDIR}inventory/mycluster/"
KUBESPRAY_CONF="${MYCLUSTER}hosts.yml"

cd ${KUBESPRAYDIR}
cp -rfp inventory/sample inventory/mycluster

# start of hosts.yml
cat << EOF | sudo tee "${KUBESPRAY_CONF}" > /dev/null
all:
  hosts:
EOF

# add hosts
for ((i = 1; i <= ${total_nodes}; i++)); do
  node_ip="$(lookup node${i})"
  cat << EOF | sudo tee -a "${KUBESPRAY_CONF}" > /dev/null
    node${i}:
      ansible_host: ${node_ip}
      ip: ${node_ip}
      access_ip: ${node_ip}
EOF
done
echo "Total Nodes: $((i - 1))"

# add control plane
cat << EOF | sudo tee -a "${KUBESPRAY_CONF}" > /dev/null
  children:
    kube_control_plane:
      hosts:
EOF
max_nodes="$(max_nodes ${MAX_CONTROL})"
for ((i = 1; i <= ${max_nodes}; i++)); do
  cat << EOF | sudo tee -a "${KUBESPRAY_CONF}" > /dev/null
        node${i}:
EOF
done
echo "Control Plane Nodes: $((i - 1))"

# add worker nodes
cat << EOF | sudo tee -a "${KUBESPRAY_CONF}" > /dev/null
    kube_node:
      hosts:
EOF

for ((i = 1; i <= ${total_nodes}; i++)); do
  cat << EOF | sudo tee -a "${KUBESPRAY_CONF}" > /dev/null
        node${i}:
EOF
done
echo "Worker Nodes: $((i - 1))"

# add etcd nodes

cat << EOF | sudo tee -a "${KUBESPRAY_CONF}" > /dev/null
    etcd:
      hosts:
EOF

max_nodes="$(max_nodes ${MAX_ETCD})"
for ((i = 1; i <= ${max_nodes}; i++)); do
  cat << EOF | sudo tee -a "${KUBESPRAY_CONF}" > /dev/null
        node${i}:
EOF
done
echo "etcd Nodes: $((i - 1))"

# additional entries
cat << EOF | sudo tee -a "${KUBESPRAY_CONF}" > /dev/null    kube_node:
    k8s_cluster:
      children:
        kube_control_plane:
        kube_node:
    calico_rr:
      hosts: {}
EOF

sed -i "s|kube_network_plugin_multus: false|kube_network_plugin_multus: true|" ${MYCLUSTER}group_vars/k8s_cluster/k8s-cluster.yml

}

lookup () {
  dig +short ${1}
}

component_count () {

  MAX_COMPONENTS=20

  for ((i = 1; i <= ${MAX_COMPONENTS}; i++)); do
    if [ -z "$(dig +short ${1}${i})" ]; then
      break
    fi
  done
  echo "$((i - 1))"
}

max_nodes () {
  total_nodes="$(component_count node)"
  if [[ ${total_nodes} -lt ${1} ]]; then
    max_nodes=${total_nodes}
  else
    max_nodes=${1}
  fi
  echo "${max_nodes}"
}

# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit


