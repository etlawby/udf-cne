#! /bin/bash

main () {
  update_routes
}

update_routes () {

INT_SELF="10.1.10.21"
EXT_SELF="10.1.20.21"

int="${1:-$INT_SELF}"
ext="${2:-$EXT_SELF}"

echo "Setting Self-ip routes, Internal:${int}, External:${ext}"

VYOS_SCRIPT="/opt/vyatta/etc/functions/script-template"

CREDS="vyos@router1"
ssh "${CREDS}" 'vbash -s' <<EOF
source ${VYOS_SCRIPT}
# router1
configure
delete protocols static route 0.0.0.0/0
set protocols static route 0.0.0.0/0 next-hop ${int}
commit
save
EOF

CREDS="vyos@router2"
ssh "${CREDS}" 'vbash -s' <<EOF
source ${VYOS_SCRIPT}
# router2
configure
delete protocols static route 10.1.10.0/24
delete protocols static route 10.1.30.0/24
delete protocols static route 100.64.0.0/10
set protocols static route 10.1.10.0/24 next-hop ${ext}
set protocols static route 10.1.30.0/24 next-hop ${ext}
set protocols static route 100.64.0.0/10 next-hop ${ext}
commit
save
EOF
}

# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
