#! /bin/bash

export DEFAULT_PASSWORD="HelloUDF"
export VYOS_SCRIPT="/opt/vyatta/etc/functions/script-template"

### vyos bug https://vyos.dev/T3871
# sudo vi /opt/vyatta/etc/config/scripts/vyatta-postconfig-bootup.script
#vyos@vyos:~$ cat /opt/vyatta/etc/config/scripts/vyatta-postconfig-bootup.script
#sudo dhclient -v e2
#sudo chmod +x /opt/vyatta/etc/config/scripts/vyatta-postconfig-bootup.script

main () {
  configure_vyos
}

configure_vyos () {

require sshpass
require_file ~/.ssh/id_rsa

CREDS="vyos@router1"
vyos_ssh_init "${CREDS}"
ssh "${CREDS}" 'vbash -s' <<EOF
source ${VYOS_SCRIPT}
# router1
configure
set system host-name router1
set interfaces ethernet eth0 dhcp-options no-default-route # IMPORTANT!
set interfaces ethernet eth1 address dhcp
set interfaces ethernet eth2 address dhcp
set protocols static route 0.0.0.0/0 next-hop 10.1.10.21
commit
save
EOF

CREDS="vyos@router2"
vyos_ssh_init "${CREDS}"
ssh "${CREDS}" 'vbash -s' <<EOF
source ${VYOS_SCRIPT}
# router2
configure
set system host-name router2
set interfaces ethernet eth0 dhcp-options no-default-route # IMPORTANT!
set interfaces ethernet eth1 address dhcp
set interfaces ethernet eth2 address dhcp
set protocols static route 0.0.0.0/0 next-hop 10.1.40.1
set protocols static route 10.1.10.0/24 next-hop 10.1.20.21
set protocols static route 10.1.30.0/24 next-hop 10.1.20.21
set protocols static route 100.64.0.0/10 next-hop 10.1.20.21
#set protocols static route 0.0.0.0/0 dhcp-interface eth2 
set nat source rule 10 outbound-interface eth2 
set nat source rule 10 translation address masquerade 
commit
save
EOF
}

vyos_ssh_init () {
  CREDS="${1}"
  USER="$(echo "${CREDS}" | sed "s|@.*$||")"
  type="$(cat ~/.ssh/id_rsa.pub | cut -f 1 -d ' ')"
  key="$(cat ~/.ssh/id_rsa.pub | cut -f 2 -d ' ')"
  sshpass -p "${DEFAULT_PASSWORD}" ssh "${CREDS}" -o StrictHostKeyChecking=accept-new "exit"
  sshpass -p "${DEFAULT_PASSWORD}" ssh "${CREDS}" 'vbash -s' <<EOF
# fix for interface renaming bug
sudo rm /etc/udev/rules.d/70-persistent-net.rules
source ${VYOS_SCRIPT}
configure
set system login user ${USER} authentication public-keys jumphost-pub-key key ${key}
set system login user ${USER} authentication public-keys jumphost-pub-key type ${type}
commit
save
exit
EOF
}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit


