#! /bin/bash

##### ~/scripts/library.sh #####

# source library.sh

LOG_FILE="/var/log/udf-cne.log"
LOG_LEVEL="debug"
LINE="------------------------------------------------------"

option () {
   parameter=$(echo $command | egrep "\--$1=")
   if [ ! -z "$parameter" ]; then
      echo "$command " | sed "s/.* --$1=\([^ ]*\).*$/\1/" 
   fi
}

option_set () {
   flag=$(echo "$command " | egrep "\--$1[ ]+")
   if [ ! -z "$flag" ]; then
      echo "$command " | sed "s/.* --$1[ ]\+.*$/true/"
   else
      echo false
   fi
}

options_valid () {
   arguments=$(echo $command | sed "s/[^ ]*//")
   for option in $arguments
   do
      baseoption=$(echo $option | sed "s/^--//" | sed "s/=.*//")
      validoption=$(echo " $* " | grep " $baseoption ")
      if [ -z "$validoption" ]; then
         log warning "unsupported option: $baseoption"
      fi
   done
}

get_component_id () {
  component="$(echo ${1} | tr -d ' ')"
  found="$(curl -s metadata.udf/deployment | jq ".deployment.components[].name" | tr -d ' ' | grep -i "${component}" | wc -l)"
  if (( ${found} == 0 )); then
    log error "UDF component <${component}> not known"
  elif (( ${found} != 1 )); then
    log error "Ambiguous UDF component <${component}>"
  else
    echo "$(curl -s metadata.udf/deployment | jq ".deployment.components[]| {id: .id, name: .name}" -c | tr -d ' ' | grep -i "${component}" | jq '.id' | tr -d '"' )"
  fi
}

get_component_ip () {
  id="$(get_component_id "${1}")"
  echo "$(curl -s metadata.udf/deployment | jq ".deployment.components[]|select(.id==\"${id}\").mgmtIp" | tr -d '"')"
}

get_interface () {
  id="$(get_component_id "${1}")"
  echo  "$(curl -s metadata.udf/deployment| jq ".deployment.components[] | select(.id==\"${id}\") |.trafficIps[].primary" | tr -d '"' | grep -i "$(echo "${2}" | sed "s|\.[^\.]*$|.|")")"
}

test_dns () {
  dns="$(ssh "${1}" "dig jumphost +search +short")"
  ip="$(get_component_ip jumphost)"
  log debug "${1} DNS:${dns}"
}

require () {
  for program in "$@"
  do
    if [[ -z "$(which "${program}")" ]]; then
      log error "${program} is not installed"
      exit 1 
    fi
  done
}

require_file () {
  if [[ ! -e ${1} ]]; then
    log error "${1} file not found"
    exit 1 
  fi
}

fatal_error () {
  log error "FATAL ERROR - ABORT"
  echo "FATAL ERROR - ABORT"
  exec bash
}

log () {
   LOG_LEVELS="|debug|info|notice|warning|error|critical|alert|emergency|"
   MAX_LOG_SIZE=100

   if [ -z "$LOG_FILE" ]; then
      LOG_FILE='/dev/null'
   fi
   if [ ! -z $(echo ${LOG_FILE} | sed "s|^/var/log/.*$||") ]; then
      LOG_FILE='/dev/null'
      VALID=' *log file invalid'
   else
      VALID=''
   fi
   if [ -z "${LOG_LEVEL}" ]; then
      LOG_LEVEL='info'
   fi
   
   log_level=$(option "log-level")
   if [ -z ${log_level} ]; then
      log_level=${LOG_LEVEL}
   fi

   log_filter=$(echo ${LOG_LEVELS^^} | sed "s/.*|\(${log_level^^}|.*\)|/|\1|/")
   scriptname=$(echo $command | cut -d' ' -f1)
   if [ ! -z $(echo ${log_filter} | egrep -i $1) ]; then
      timesync=$(timedatectl status | grep "System clock synchronized:" | cut -d":" -f2 | tr -d ' ')
      if [ "$timesync" != "yes" ]; then
         sync_status=" [unsync'd]"
      fi
      message="${1^^} ${2}${VALID}"
#      echo $message
      message="$(date)${sync_status} $message ($scriptname)"
      echo $message | sudo tee -a $LOG_FILE > /dev/null
   fi
}

component_count () {

  MAX_COMPONENTS=20

  for ((i = 1; i <= ${MAX_COMPONENTS}; i++)); do
    if [ -z "$(dig +short ${1}${i})" ]; then
      break
    fi
  done
  echo "$((i - 1))"
}

max_nodes () {
  total_nodes="$(component_count node)"
  if [[ ${total_nodes} -lt ${1} ]]; then
    max_nodes=${total_nodes}
  else
    max_nodes=${1}
  fi
  echo "${max_nodes}"
}

