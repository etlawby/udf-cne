#! /bin/bash

main () {
  deploy_cnf
}

deploy_cnf () {
wget -xnH -O - repo.secure-demo.net/cnf-boot | bash

timeout="300"
start_time=$(date +%s)
log info "CNF test started"

while true; do
  response="$(ssh ubuntu@client "curl -s web1.cnf.local")"
  if [[ -n "$(echo "${response}" | grep "^Server ")" ]]; then
    echo "${response}"
    break
  fi
  current_time=$(date +%s)
  elapsed_time=$((current_time - start_time))
  if [ $elapsed_time -ge $timeout ]; then
    echo "No response after ${timeout}s"
    fatal_error
  fi
  sleep 10
done

log info "Deployment completed successfully"

}

# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit

