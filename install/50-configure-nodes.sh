#! /bin/bash

LOCAL_DOMAIN="udf"

main () {
  configure_nodes
}

configure_nodes () {

total_nodes="$(component_count node)"
echo "Total Nodes: ${total_nodes}"

for ((i = 1; i <= ${total_nodes}; i++)); do
  update_node node${i}
  wait_for_ssh node${i}
  sleep 5;
  test_dns "ubuntu@node${i}"
done

}

update_node () {
  CREDS="ubuntu@${1}"
  DNS="$(get_component_ip jumphost)"
  ssh -q -o PreferredAuthentications=publickey ${CREDS} exit
  if [ ${?} -eq 0 ]; then
    ssh "${CREDS}" > /dev/null 2>&1 <<EOF
sudo apt update
sudo NEEDRESTART_SUSPEND=1 apt -y install linux-modules-extra-5.15.0-1041-azure
echo 'vm.nr_hugepages=4096' | sudo tee -a /etc/sysctl.conf > /dev/null
sudo sed -i 's|GRUB_CMDLINE_LINUX=\"\"|GRUB_CMDLINE_LINUX=\"systemd.unified_cgroup_hierarchy=0\"|' /etc/default/grub
sudo update-grub
sudo netplan set ethernets.ens5.dhcp4-overrides.use-dns=false
sudo netplan set ethernets.ens5.nameservers.addresses=["'"${DNS}"'"]
if ip link show "ens6" &>/dev/null; then sudo netplan set ethernets.ens6.dhcp4=true; fi
if ip link show "ens7" &>/dev/null; then sudo netplan set ethernets.ens7.dhcp4=true; fi
if ip link show "ens8" &>/dev/null; then sudo netplan set ethernets.ens8.dhcp4=true; fi
sudo netplan apply
sudo sed -i "s|^#*DNS=.*$|DNS="${DNS}"|" /etc/systemd/resolved.conf
sudo sed -i "s|^#*Domains=.*$|Domains=udf|" /etc/systemd/resolved.conf
sudo systemctl restart systemd-resolved
#ip_tables loaded by default, ip6_tables not required?
#echo 'ip_tables' | sudo tee -a /etc/modules > /dev/null
#echo 'ip6_tables' | sudo tee -a /etc/modules > /dev/null
sudo reboot
EOF
    log debug "${1} configured"
  else
    log warning "${1} Public Key not available"
  fi
}

wait_for_ssh () {
  host="${1}"
  port="22"
  timeout="300"
  echo "Waiting for port ${port} on host ${host}"
  start_time=$(date +%s)
  while true; do
    ssh -q -o PreferredAuthentications=publickey ${1} exit
    if [ ${?} -eq 0 ]; then
      echo "Port ${port} on host ${host} is open."
      break
    fi
    current_time=$(date +%s)
    elapsed_time=$((current_time - start_time))
    if [ $elapsed_time -ge $timeout ]; then
        echo "Error: Timeout reached. Port ${port} on host ${host} is still closed after ${timeout} seconds."
        break
    fi
    sleep 1
  done
}

component_count () {

  MAX_COMPONENTS=20

  for ((i = 1; i <= ${MAX_COMPONENTS}; i++)); do
    if [ -z "$(dig +short ${1}${i})" ]; then
      break
    fi
  done
  echo "$((i - 1))"
}

# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
