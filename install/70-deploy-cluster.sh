#! /bin/bash

main () {
  deploy_cluster
}

# https://dev.to/admantium/kubernetes-installation-tutorial-kubespray-46ek

deploy_cluster () {

cd ~/.
KUBESPRAYDIR=$(pwd)/kubespray/
VENVDIR="${KUBESPRAYDIR}.venv/"
MYCLUSTER="${KUBESPRAYDIR}inventory/mycluster/"
virtualenv --python=$(which python3) ${VENVDIR}
source ${VENVDIR}bin/activate
cd ${KUBESPRAYDIR}

version="$(cat ${MYCLUSTER}group_vars/k8s_cluster/k8s-cluster.yml | grep "^kube_version: ")"
log info "Deploying ${version}"

ansible-playbook -i ${MYCLUSTER}hosts.yml --private-key=~/.ssh/id_rsa -u ubuntu --become-user=root --become ${KUBESPRAYDIR}cluster.yml

mkdir -p ~/.kube
ssh ubuntu@node1 "sudo cat /etc/kubernetes/admin.conf" > ~/.kube/config
sed -i "s|127.0.0.1|node1|" ~/.kube/config 
chmod 600 ~/.kube/config

kubectl get nodes
}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit

