#! /bin/bash

main () {
  uninstall_vyos
}

uninstall_vyos () {

CREDS="vyos@router1"
vyos_ssh_uninit "${CREDS}"

CREDS="vyos@router2"
vyos_ssh_uninit "${CREDS}"

}

vyos_ssh_uninit () {
  ssh -q -o PreferredAuthentications=publickey StrictHostKeyChecking=no ${1} exit
  if [ ${?} -eq 0 ]; then
    ssh "${CREDS}" 'vbash -s' <<"EOF"
source /opt/vyatta/etc/functions/script-template
configure
load "$(ls /config/*.pre-migration)"
commit
save
exit
EOF
  else
    log warning "${1} Public Key not available"
  fi
}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit

