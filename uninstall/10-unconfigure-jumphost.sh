#! /bin/bash


main () {
  unconfigure_jumphost
}

### unconfigure jumphost ###

unconfigure_jumphost () {

kubectl krew uninstall virt
sed -i '/export PATH="${KREW_ROOT:-$HOME\/.krew}\/bin:$PATH"/d' ~/.bashrc 
rm -rf -- "${KREW_ROOT:-$HOME/.krew}"

pip3 remove ansible==9.13.0
pip3 remove netaddr
sudo snap remove kubectl
sudo snap remove helm
sudo snap remove jq
sudo snap remove jo
sudo snap remove yq
sudo apt update
sudo NEEDRESTART_MODE=a apt remove snap sshpass git ansible python3 python3-pip python3-virtualenv dnsmasq dnsutils -y

rm -rf ~/snap/
rm -rf ~/udf-cne/
rm ~/.ssh/id_rsa*
rm ~/.ssh/known_hosts
rm ~/.bash_aliases
sudo ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
sudo systemctl restart systemd-resolved

sudo hostnamectl hostname ubuntu

#curl -sS https://webinstall.dev/k9s | bash
#source ~/.config/envman/PATH.env

}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit

