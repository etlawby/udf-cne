#! /bin/bash

main () {
  undeploy_nginx
}

### configure nginx ###

undeploy_nginx () {

uninstall_nginx "ubuntu@client"
uninstall_nginx "ubuntu@server"

}

uninstall_nginx () {

  NGINX_CONF="/etc/nginx/sites-available/my-nginx.conf"
  CREDS="${1}"
  ssh -q -o PreferredAuthentications=publickey StrictHostKeyChecking=no ${CREDS} exit
  if [ ${?} -eq 0 ]; then
    ssh "${CREDS}" > /dev/null 2>&1 <<EOF
sudo NEEDRESTART_SUSPEND=1 apt -y remove nginx fcgiwrap
sudo rm -rf /etc/nginx
sudo rm -rf /usr/share/nginx
EOF
  else
    log warning "${1} Public Key not available"
  fi
}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
