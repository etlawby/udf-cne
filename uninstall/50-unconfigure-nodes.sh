#! /bin/bash

main () {
  unconfigure_nodes
}

unconfigure_nodes () {

update_node node1
update_node node2
update_node node3

wait_for node1
wait_for node2
wait_for node3

}

update_node () {
  CREDS="ubuntu@${1}"
  ssh -q -o PreferredAuthentications=publickey StrictHostKeyChecking=no ${CREDS} exit
  if [ ${?} -eq 0 ]; then
    ssh "${CREDS}" > /dev/null 2>&1 <<EOF
sudo apt update
sudo NEEDRESTART_SUSPEND=1 apt -y remove linux-modules-extra-5.15.0-1041-azure
sudo -i '/vm.nr_hugepages=4096/d' /etc/sysctl.conf
sudo sed -i 's|GRUB_CMDLINE_LINUX=\"systemd.unified_cgroup_hierarchy=0\"|GRUB_CMDLINE_LINUX=\"\"' /etc/default/grub
sudo update-grub
sudo netplan set ethernets.ens6.dhcp4=false
sudo netplan set ethernets.ens7.dhcp4=false
sudo netplan apply"
#echo 'ip_tables' | sudo tee -a /etc/modules > /dev/null
#echo 'ip6_tables' | sudo tee -a /etc/modules > /dev/null
sudo reboot
EOF
    log debug "${1} unconfigured"
  else
    log warning "${1} Public Key not available"
  fi
}

wait_for () {

host="${1}"
port=22
timeout=300
start_time=$(date +%s)

echo "Waiting for port ${port} on host ${host}"

while true; do
    nc -z -w 1 "${host}" "${port}" &> /dev/null
    result=$?
    if [ ${result} -eq 0 ]; then
        echo "Port ${port} on host ${host} is open."
        break
    fi
    current_time=$(date +%s)
    elapsed_time=$((current_time - start_time))
    if [ $elapsed_time -ge $timeout ]; then
        echo "Error: Timeout reached. Port ${port} on host ${host} is still closed after ${timeout} seconds."
        break
    fi

    sleep 1
done

}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
