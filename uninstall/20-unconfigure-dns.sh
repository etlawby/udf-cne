#! /bin/bash

main () {
  unconfigure_dns
}

unconfigure_dns () {

sudo sed -i "s|^127.0.0.1 .*$|127.0.0.1 localhost localhost.localdomain|" /etc/hosts
sudo sed -i "/^###/q" /etc/hosts
sudo sed -i "/^###/d" /etc/hosts
sudo sed -i "/^10\./d" /etc/hosts

sudo sed -i "s|^#*DNSStubListenerExtra=.*$|#DNSStubListenerExtra=|" /etc/systemd/resolved.conf
sudo systemctl restart systemd-resolved

}

delete_hosts_entry () {
  entry="${*}"
  sudo sed -i "/${entry}/d" /etc/hosts
}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
