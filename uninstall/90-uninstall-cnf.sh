#! /bin/bash

main () {
  uninstall_cnf
}

### configure nginx ###

uninstall_cnf() {
  log info "uninstall CNF not enabled"
}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
