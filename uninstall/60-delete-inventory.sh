#! /bin/bash

main () {
  delete_inventory
}


delete_inventory () {

cd ~/.
KUBESPRAYDIR=$(pwd)/kubespray
rm -rf ${KUBESPRAYDIR}

}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
