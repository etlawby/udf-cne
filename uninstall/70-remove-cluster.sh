#! /bin/bash

main () {
  remove_cluster
}

remove_cluster () {

cd ~/.
KUBESPRAYDIR=$(pwd)/kubespray/
VENVDIR="${KUBESPRAYDIR}.venv/"
MYCLUSTER="${KUBESPRAYDIR}inventory/mycluster/"
source ${VENVDIR}bin/activate
cd ${KUBESPRAYDIR}

ansible-playbook -i ${MYCLUSTER}hosts.yml --private-key=~/.ssh/id_rsa -u ubuntu --become-user=root --become ${KUBESPRAYDIR}reset.yml --extra-var "reset_confirmation=yes"
rm -rf ~/.kube

}


# end of script:
# this enables main function to be at top of file
export command="$0 $*"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)/"
source ${SCRIPT_DIR}library.sh
log info "started"
main "$@"
log info "completed"
exit
