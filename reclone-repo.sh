#! /bin/bash

# re-pull (update) the local repo

cd ~/.
rm -rf ~/udf-cne
git clone https://gitlab.com/etlawby/udf-cne
ln -s ~/udf-cne/install/library.sh ~/udf-cne/uninstall/library.sh 
