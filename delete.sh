#! /bin/bash

kubectl delete secret far-secret -n default
kubectl delete networkattachmentdefinition.k8s.cni.cncf.io client-net -n default
kubectl delete networkattachmentdefinition.k8s.cni.cncf.io server-net -n default
kubectl delete spkinfrastructure.charts.k8s.f5net.com spkinfrastructure-sample -n default
kubectl delete spkinstance.charts.k8s.f5net.com spkinstance-sample -n default

kubectl delete -f ~/udf-cne/olm/spkinstance.yaml
kubectl delete -f ~/udf-cne/olm/spkinfrastructure.yaml

helm uninstall cert-manager -n cert-manager
helm uninstall orchestrator -n default

kubectl delete deploy f5-tmm -n default 
kubectl delete deploy spkinstance-sample-f5ingress -n default 

kubectl delete crd certificaterequests.cert-manager.io
kubectl delete crd certificates.cert-manager.io
kubectl delete crd challenges.acme.cert-manager.io
kubectl delete crd clusterissuers.cert-manager.io
kubectl delete crd issuers.cert-manager.io
kubectl delete crd orders.acme.cert-manager.io

kubectl get crds | grep "^f5-" | sed "s|^|kubectl delete crd |" | bash

kubectl delete clusterrole orchestrator
kubectl delete clusterrolebinding orchestrator
kubectl delete clusterrolebinding cert-manager-cainjector
kubectl delete clusterrolebinding cert-manager-controller-approve:cert-manager-io                        
kubectl delete clusterrolebinding cert-manager-controller-certificates                                   
kubectl delete clusterrolebinding cert-manager-controller-certificatesigningrequests                    
kubectl delete clusterrolebinding cert-manager-controller-challenges                                   
kubectl delete clusterrolebinding cert-manager-controller-clusterissuers                                
kubectl delete clusterrolebinding cert-manager-controller-ingress-shim                                  
kubectl delete clusterrolebinding cert-manager-controller-issuers                                      
kubectl delete clusterrolebinding cert-manager-controller-orders                                        
kubectl delete clusterrolebinding cert-manager-webhook:subjectaccessreviews

kubectl label node node1 type-
kubectl label node node2 type-
kubectl label node node3 type-
kubectl label node node4 type-
kubectl label node node5 type-
kubectl label node node4 app-
kubectl label node node5 app-
kubectl taint nodes node4 dpu-
kubectl taint nodes node5 dpu-

NS="cert-manager"
kubectl get namespace "${NS}" -o json | jq '.spec.finalizers=[]' | kubectl replace --raw "/api/v1/namespaces/${NS}/finalize" -f -
kubectl delete ns "${NS}"

NS="f5-utils"
kubectl get namespace "${NS}" -o json | jq '.spec.finalizers=[]' | kubectl replace --raw "/api/v1/namespaces/${NS}/finalize" -f -
kubectl delete ns "${NS}"

NS="my-bnk"
kubectl get namespace "${NS}" -o json | jq '.spec.finalizers=[]' | kubectl replace --raw "/api/v1/namespaces/${NS}/finalize" -f -
kubectl delete ns "${NS}"

NS="app-ns"
kubectl get namspace "${NS}" -o json | jq '.spec.finalizers=[]' | kubectl replace --raw "/api/v1/namespaces/${NS}/finalize" -f -
kubectl delete ns "${NS}"
