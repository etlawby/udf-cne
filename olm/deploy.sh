#! /bin/bash

# alias oc="kubectl"
oc () {
  kubectl "$@"
}


NS="default"
sed -i "s|namespace: default|namespace: ${NS}|" spkinfrastructure.yaml
sed -i "s|namespace: default|namespace: ${NS}|" spkinstance.yaml


kubectl label node node1 type=f5-control
kubectl label node node2 type=worker
kubectl label node node3 type=worker
kubectl label node node4 type=dpu
kubectl label node node5 type=dpu
kubectl label node node4 app=f5-tmm
kubectl label node node5 app=f5-tmm
kubectl taint nodes node4 dpu=true:NoSchedule
kubectl taint nodes node5 dpu=true:NoSchedule
#kubectl get nodes --show-labels




# https://docs.f5net.com/pages/viewpage.action?spaceKey=~booli&title=Operator+for+OCP+on+UDF

# Install cert-manager
helm repo add jetstack https://charts.jetstack.io --force-update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.16.1  --set crds.enabled=true

# Create selfsigned CA and cluster issuer
oc apply -f cert-manager-cluster-issue.yaml

# Log into FAR
cat dev_pull_64.json | helm registry login -u _json_key_base64 --password-stdin https://devrepo.f5.com

# Create f5-utils namespace and application namespace for f5ingress to watch for
oc create ns f5-utils
#oc create ns "${NS}"

#Create secrets for FAR access
oc apply -f far-secret.yaml -n "${NS}"
oc apply -f far-secret.yaml -n f5-utils

# Create CPCL key configmap
oc apply -f cpcl-key.yaml -n f5-utils

# Create network attachment definition
oc apply -f macvlan2.yaml -n "${NS}"
# Adjust the content based on actual environment.

# Install Operator
helm install orchestrator oci://devrepo.f5.com/charts/orchestrator  -f orchestrator-values.yaml --version v0.0.46-ocp-fix-main.12475678 -n "${NS}"

# Create spk infrastructure CR
oc apply -f spkinfrastructure.yaml
# Adjust networkAttachment based on actual resource names.

# Create spk instance CR
oc apply -f spkinstance.yaml

# Check pod status

# If you have license issue, you can create a secret in f5-utils/cwc namespace.
oc apply -f lic-secret.yaml

# Install VLANs
# Install application pod in 'app-ns' namespace
# Install ingresstcp