#! /bin/bash

cd ~/udf-cne/olm
bash ~/udf-cne/k8s/create-internal-links.sh 

NS="default"
sed -i "s|namespace: default|namespace: ${NS}|" my-spkinfrastructure.yaml
sed -i "s|namespace: default|namespace: ${NS}|" my-spkinstance.yaml
kubectl create ns app-ns

kubectl label node node1 type=f5-control
kubectl label node node2 type=worker
kubectl label node node3 type=worker
kubectl label node node4 type=dpu
kubectl label node node5 type=dpu
kubectl label node node4 app=f5-tmm
kubectl label node node5 app=f5-tmm
kubectl taint nodes node4 dpu=true:NoSchedule
kubectl taint nodes node5 dpu=true:NoSchedule
#kubectl get nodes --show-labels


# https://docs.f5net.com/pages/viewpage.action?spaceKey=~booli&title=Operator+for+OCP+on+UDF

# Install cert-manager
helm repo add jetstack https://charts.jetstack.io --force-update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.16.1  --set crds.enabled=true

kubectl apply -f cert-manager-cluster-issue.yaml
cat dev_pull_64.json | helm registry login -u _json_key_base64 --password-stdin https://devrepo.f5.com
kubectl create ns f5-utils
kubectl apply -f far-secret.yaml -n f5-utils
kubectl apply -f far-secret.yaml -n "${NS}"
kubectl apply -f cpcl-key.yaml -n f5-utils
kubectl apply -f my-macvlan.yaml -n "${NS}"

helm install orchestrator oci://devrepo.f5.com/charts/orchestrator  -f my-orchestrator-values.yaml --version v0.0.46-ocp-fix-main.12475678 -n "${NS}"

kubectl apply -f my-spkinfrastructure.yaml
kubectl apply -f my-spkinstance.yaml
kubectl apply -f lic-secret.yaml

