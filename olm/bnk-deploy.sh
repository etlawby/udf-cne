#! /bin/bash

# https://docs-70d623.pages.gitswarm.f5net.com/latest/install-bnk/

# clean up
helm uninstall orchestrator

# remove finalizers and delete crds
for crd in $(kubectl get crds -o custom-columns=NAME:.metadata.name,API_GROUP:.spec.group | grep charts.k8s.f5net.com | awk '{print $1}'); do
    echo "$crd"
    kubectl patch crds "$crd" -p '{"metadata":{"finalizers":null}}' --type=merge
    kubectl delete crds "$crd"
done

kubectl label node node1 type-
kubectl label node node2 type-
kubectl label node node3 type-
kubectl label node node4 type-
kubectl label node node5 type-
kubectl label node node4 app-
kubectl label node node5 app-
kubectl taint nodes node4 dpu-
kubectl taint nodes node5 dpu-

bash ~/udf-cne/k8s/create-internal-links.sh 

kubectl create ns app-ns

kubectl label node node1 type=f5-control
kubectl label node node2 type=worker
kubectl label node node3 type=worker
kubectl label node node4 type=dpu
kubectl label node node5 type=dpu
kubectl label node node4 app=f5-tmm
#kubectl label node node5 app=f5-tmm
#kubectl taint nodes node4 dpu=true:NoSchedule
#kubectl taint nodes node5 dpu=true:NoSchedule

# Install cert-manager

cat <<EOF > cert-manager-values.yaml # added values for node selection
nodeSelector:
  type: f5-control
webhook:
  nodeSelector:
    type: f5-control
cainjector:
  nodeSelector:
    type: f5-control
startupapicheck:
  nodeSelector:
    type: f5-control
crds:
  enabled: true
EOF

kubectl create ns cert-manager
helm repo add jetstack https://charts.jetstack.io --force-update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --values cert-manager-values.yaml --version v1.16.1

# arm --> bnk
cat <<EOF > cert-manager-cluster-issue.yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: selfsigned-cluster-issuer
spec:
  selfSigned: {}
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: bnk-ca
  namespace: cert-manager
spec:
  isCA: true
  commonName: bnk-ca
  secretName: bnk-ca
  issuerRef:
    name: selfsigned-cluster-issuer
    kind: ClusterIssuer
    group: cert-manager.io
---
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: bnk-ca-cluster-issuer
spec:
  ca:
    secretName: bnk-ca
EOF

kubectl apply -f cert-manager-cluster-issue.yaml

kubectl taint node node4 dpu=true:NoSchedule
kubectl taint node node5 dpu=true:NoSchedule

kubectl label node node4 app=f5-tmm
kubectl label node node5 app=f5-tmm

kubectl create ns f5-utils

for ns in red blue; do kubectl create ns $ns; done

cat <<EOF > cne_pull_64.json
ewogICJ0eXBlIjogInNlcnZpY2VfYWNjb3VudCIsCiAgInByb2plY3RfaWQiOiAiZjUtZ2NzLTcwNTYtcHRnLWNuZS1wcm9kIiwKICAicHJpdmF0ZV9rZXlfaWQiOiAiNjE0MTdiOTlmYTY1MjkwODI3MGVmNWU1NGFlNWJhMjYyYWYwYWE4NCIsCiAgInByaXZhdGVfa2V5IjogIi0tLS0tQkVHSU4gUFJJVkFURSBLRVktLS0tLVxuTUlJRXZRSUJBREFOQmdrcWhraUc5dzBCQVFFRkFBU0NCS2N3Z2dTakFnRUFBb0lCQVFEaldjSEhibC8xODlRWVxueXVEb2orekxMVXpSUytsWHRqUmxqMXRkMEZ4UlVhbHhJdllEclpuN3ZmMlpYODRxbHZlUmNOaUhkQ0hIVWMrcVxuc3VaaDNteGVHSENUajRXOEpFMnF4RHg4Tm5zN3BzcXZ3R2tDb1JpZ3JTcEYxQThRWTB0c1J1S0ZSZDdJV3p4ZVxuY3JNaVV1SG12VTA4aXprd0FkT3d2MkJyMittdFFWYWZFZ290dFRzaGVzVzVibHlzNjZoenNCTlRlWEpZb3FOc1xubGdIVDMvUkZPQWtaQ2kzdTBUYlNjalRrYjZMMEhPOGxLY0E5eDc1WmhBZzhYbEppb05UUDlOT0l5TGpsQVJnQVxuZG9VV25TS1B2Q1Qvb0xPU2luamRXRE9FTmFORUExK01nWTIvSmxaQktXTWR3RFRRRUtnL1BBYVUwelhyN3ZFbVxuNWM5OVdoZmRBZ01CQUFFQ2dnRUFIdnZYQkZwZStwSjV4ODB5WEk5c3luM0lkOGVjSytkNWVaUFJkRllXbHNDRlxuMHBUY3FVQ3ArUng1Nk1LZFFzR2pTSzl6MnZYeE9veEVQSE80SVFOaWltcFRRVkR3ZldVSExxd2l3QnplcTVEK1xuRTNpcDl3RkViWFZjQ3ZzcytHTkgzcUJFYzd4RGNkTzVSUGZsd05wQnppUGpXRXB6Qno5Z2ZiaC9LQVBtODBUdlxuOVI2SjVkUDA4WUQ1QWMyNjA1elUreFA2RnRxY1ErOHZ0cWoyaDcrSjRjQUVKUkZYZ2kwWUlmRDBIaHBkREVBYlxud2VTMXdibitzMkFiWkhGSVhXSXQ4MUxsWGlOSVVJcnhqRGFTSWJOR0VVUjNXa3FKR3ovOFpDS2V1NW9wY3RlcVxuMHdqZnF2eXBLdW9RMkpveDJzVzFZRC84ZU9Fc2F6U3hUam5KeHZ1MjR3S0JnUUQ5MDFPalowcW80RDBXcHFQalxuOXNQd3dTUEh6b2xTY2hCVUhFb3Y3dmNkMTU1dU1YZmFnZmZzVTkwaTdZUkxuL0Z1L3VJemdjRk1mODBobk1BM1xuNHhXZE5kc2tvSWJFMEVGMDdsZXRHVGVZQXQ3ODNCcFRMYTVHVFc0a054TkRKSWUrRlRGUWdCWGFNblVSSm5YNFxuNXR4Mks4aHliSVdId1ZCTDRnd3NISGhmU3dLQmdRRGxURjRGNG8yRWYzaEl4TWRpOWczS0lVMHdnbmVmcGdvQVxuSnViK1pyRVpkaS80MjdENmRMcGlST0hNV3hUUHhBa2E4UngraFFkZE1EenZFQzlYcU5FU25ON2tDRXhRKzBaRFxuTzU3YTlvVTBxVjR0azhrQmRJemVwaUNqRzZNUnNBb1g2SkZwaFF5TnF4K3lkOWN1LzloMDZ4azJxZlJiNzl1eVxuVTd6WXRqWGtkd0tCZ0FWQ2NjTTdDck1CSW9McnBRMitPVVV0MjEzeWk2bm5yeXJ2QXlLSEl4dTlNZzA2ZlM5TFxuN2lVNWY2V1BvbVFteGt2MGdCOXRHdUpJNjZrOHBFY0VMNWg0V3pnaEUyblVudWFiWWJtMmdnNW1VTlNRZ3dNaVxuT1RBL3ZuNkZuYUtRaVRlSkhPSjhKUGZHZitLQkY0UzIwKzZtN1pwaFlVcjAySU1hQmdLTC9IcU5Bb0dBSldDVVxuZHdzZUhIb1c1bTNGUHJsek4vN0RzV09qRWxBamt6REVPTFlPcW1MY29RbFd5UXpjRVFhL2MrdG84aEUzbHIrMFxuOEY3SSs0VFFlZnUrYStDbDdBM01PNG1xdnd5bXdIRG9qVExnTmR4aDhMWUZid3BMcUVPV1VHQkI3YkV2aEFReVxuSit3N1JxQ2RjeDhiNFpwczgyb2JXeERMTjIxYTk2KzB1ZkxCeURNQ2dZRUE5b1QzYmpKdlZ0b1d5aVlucGdxd1xucDJybEZKeG5tUndDelgyWnhTTSs1dllCT0V0YmFuSWZCMFQzYXprSUN1NlR6WWlHMUxWY2ZzL2wyRFZwdWNETFxuTHA1NzR2MS9YOXgrTmdJVGJ2bitIMkQ0cUtyaUlGVUptUjF5Z1RZWDV6VE52UjZFcVZRZnRRVGY4eXNkM1VCclxucWd1Zm8yY3Vtc1hXUGRodmI0OEVxQkE9XG4tLS0tLUVORCBQUklWQVRFIEtFWS0tLS0tXG4iLAogICJjbGllbnRfZW1haWwiOiAiY25lLWdhci1wdWxsQGY1LWdjcy03MDU2LXB0Zy1jbmUtcHJvZC5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsCiAgImNsaWVudF9pZCI6ICIxMDE5OTUxNjIzMTc2NjIyMjA2MjMiLAogICJhdXRoX3VyaSI6ICJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20vby9vYXV0aDIvYXV0aCIsCiAgInRva2VuX3VyaSI6ICJodHRwczovL29hdXRoMi5nb29nbGVhcGlzLmNvbS90b2tlbiIsCiAgImF1dGhfcHJvdmlkZXJfeDUwOV9jZXJ0X3VybCI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9vYXV0aDIvdjEvY2VydHMiLAogICJjbGllbnRfeDUwOV9jZXJ0X3VybCI6ICJodHRwczovL3d3dy5nb29nbGVhcGlzLmNvbS9yb2JvdC92MS9tZXRhZGF0YS94NTA5L2NuZS1nYXItcHVsbCU0MGY1LWdjcy03MDU2LXB0Zy1jbmUtcHJvZC5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsCiAgInVuaXZlcnNlX2RvbWFpbiI6ICJnb29nbGVhcGlzLmNvbSIKfQo=
EOF

# Read the content of cne_pull_64.json into the SERVICE_ACCOUNT_KEY variable
SERVICE_ACCOUNT_KEY=$(cat cne_pull_64.json)
# Create the SERVICE_ACCOUNT_K8S_SECRET variable by appending "_json_key_base64:" to the base64 encoded SERVICE_ACCOUNT_KEY
SERVICE_ACCOUNT_K8S_SECRET=$(echo "_json_key_base64:${SERVICE_ACCOUNT_KEY}" | base64 -w 0)
# Create the secret.yaml file with the provided content
cat << EOF > far-secret.yaml
---
apiVersion: v1
kind: Secret
metadata:
  name: far-secret
data:
  .dockerconfigjson: $(echo "{\"auths\": {\
\"repo.f5.com\":\
{\"auth\": \"$SERVICE_ACCOUNT_K8S_SECRET\"}}}" | base64 -w 0)
type: kubernetes.io/dockerconfigjson
EOF

kubectl -n f5-utils apply -f far-secret.yaml
kubectl -n default apply -f far-secret.yaml

cat cne_pull_64.json | helm registry login -u _json_key_base64 --password-stdin https://repo.f5.com

# added sudo
sudo apt-get install -y make
helm pull oci://repo.f5.com/utils/f5-cert-gen --version 0.9.1
tar zxvf f5-cert-gen-0.9.1.tgz
sh cert-gen/gen_cert.sh -s=api-server -a=f5-spk-cwc.f5-utils -n=1
kubectl apply -f cwc-license-certs.yaml -n f5-utils
kubectl apply -f cwc-license-client-certs.yaml -n f5-utils

cat <<EOF > cwc-qkview-cm.yaml 
apiVersion: v1
kind: ConfigMap
metadata:
   name: cwc-qkview-cm
   namespace: f5-utils
EOF

kubectl apply -f cwc-qkview-cm.yaml 

cat <<EOF > cpcl-non-prod.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: cpcl-key-cm
  namespace: f5-utils
data:
  jwt.key: |
    {
        "keys": [
            {
                "kid": "v1",
                "alg": "RS512",
                "kty": "RSA",
                "n": "26FcA1269RC6WNgRghIB7X772zTTts02NsqqN-_keSz5FVq1Ekg151NFu_53Tgz1FGsUiX4OUj-fOmuhK9uzkQv0zYZgXY6zmRo_9P-QgiycuFo7DWquDwEx4rZiMxXwlA9ER56s8PDdbXyfi3ceMV-aUQZFqMiU6gOTl5d7uMfskocPF4ja8ZRrLlXAzzRIR62VgbQa-3sT0_SZ4w1ME4eLzO1yb-Ex9va4JnwToVLSKfsZp6jYs9nvAGjZ8aN2_lzBx8uiZ1HQozGqcf0AEjU-FEY73Umvmyvzd4woQLQlbvyrRtL9_IkL2ySdQ9Znh2lXBdsmA9cLz4ZAYPdmvcjsyBaZmh15EOkczpVVan1_VVD4o28uLDpzQVDk_GNUYoZIRsuOzuKvzih0gkv-StH29umHbdKXrUhlMWM1zyaxz8gkHatn-g5uh70WwVwqPtfHaNrQ0fFiWoyGVOA_-XqsJWA9NLJorewp9HOVlyF8qzu5s9cFO4UGQas0fF2QR9QvhgCymK7iWbEFF3PXqUQTLfFsITgix3mmeXVYC3ODsPKvcFhNBqQxmeXM04N2XMLluz2qp581NUJygWAAfq7la0ylDJ1MtefyESD8SBs1at2a8kSEBJCdCtAuNX2q33JjxQP3AiGvHcKEAjd1uaNeSgdHC93BzT3u0gbh2Ok",
                "e": "AQAB",
                "x5c": [
                    "MIIFqDCCBJCgAwIBAgIRAK+LbrS2gkaJSeoUQpMK0LswDQYJKoZIhvcNAQELBQAwgacxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApXYXNoaW5ndG9uMRowGAYDVQQKDBFGNSBOZXR3b3JrcywgSW5jLjEeMBwGA1UECwwVQ2VydGlmaWNhdGUgQXV0aG9yaXR5MTUwMwYDVQQDDCxGNSBTVEcgSXNzdWluZyBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkgVEVFTSBWMTEQMA4GA1UEBwwHU2VhdHRsZTAeFw0yMTEwMTEyMzI0NTFaFw0yNjEwMTEwMDI0NTFaMIGBMQswCQYDVQQGEwJVUzETMBEGA1UECAwKV2FzaGluZ3RvbjEQMA4GA1UEBwwHU2VhdHRsZTEaMBgGA1UECgwRRjUgTmV0d29ya3MsIEluYy4xDTALBgNVBAsMBFRFRU0xIDAeBgNVBAMMF0Y1IFNURyBURUVNIEpXVCBBdXRoIHYxMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA26FcA1269RC6WNgRghIB7X772zTTts02NsqqN+/keSz5FVq1Ekg151NFu/53Tgz1FGsUiX4OUj+fOmuhK9uzkQv0zYZgXY6zmRo/9P+QgiycuFo7DWquDwEx4rZiMxXwlA9ER56s8PDdbXyfi3ceMV+aUQZFqMiU6gOTl5d7uMfskocPF4ja8ZRrLlXAzzRIR62VgbQa+3sT0/SZ4w1ME4eLzO1yb+Ex9va4JnwToVLSKfsZp6jYs9nvAGjZ8aN2/lzBx8uiZ1HQozGqcf0AEjU+FEY73Umvmyvzd4woQLQlbvyrRtL9/IkL2ySdQ9Znh2lXBdsmA9cLz4ZAYPdmvcjsyBaZmh15EOkczpVVan1/VVD4o28uLDpzQVDk/GNUYoZIRsuOzuKvzih0gkv+StH29umHbdKXrUhlMWM1zyaxz8gkHatn+g5uh70WwVwqPtfHaNrQ0fFiWoyGVOA/+XqsJWA9NLJorewp9HOVlyF8qzu5s9cFO4UGQas0fF2QR9QvhgCymK7iWbEFF3PXqUQTLfFsITgix3mmeXVYC3ODsPKvcFhNBqQxmeXM04N2XMLluz2qp581NUJygWAAfq7la0ylDJ1MtefyESD8SBs1at2a8kSEBJCdCtAuNX2q33JjxQP3AiGvHcKEAjd1uaNeSgdHC93BzT3u0gbh2OkCAwEAAaOB8jCB7zAJBgNVHRMEAjAAMB8GA1UdIwQYMBaAFLDdK33QD9FdLnrVFw+ZAkQUayxCMB0GA1UdDgQWBBQw/hNgf2AoJAF086NV7JGQj+B2NzAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMHMGA1UdHwRsMGowaKBmoGSGYmh0dHA6Ly9jcmwtdGVlbS1zdGctb3JlLWY1LnMzLnVzLXdlc3QtMi5hbWF6b25hd3MuY29tL2NybC85ZGFmNGVlNy1iOGNkLTRiODEtOWE0MC00YjU3MGY0N2VhYWUuY3JsMA0GCSqGSIb3DQEBCwUAA4IBAQApzkSnsfuNSMHxVmL78pOQ+Rxkz1uYSVT0k1W45iufVmP0ixd8hFPcfb8u1RoHZ/58Gl52JPCudAB2sc4k/lHNT9cKL4w5F8LybB8uNJXikAqzu4HFobRYMiPtVQ7M8cFz5SgvGclxzBAbZzK5u5xZuGSkI6tG9l+D5JhW2LesRuQSBQniBgRhmtAJB7SXuZ2sNKsq04h7DWcpdjCSferymeCOLQcgy5F3ragKML8zyuNeKqtvZnUzJElKoU8G+Oo7MQXO7P5n6HX0NLfqqisv8CfSJUZTa1IRcfFUDJrcHtCgzingalzLLKzyelqR+YeY+j21jwVdnDVZIkFid2He",
                    "MIIFDjCCAvagAwIBAgIBBTANBgkqhkiG9w0BAQsFADCBhDELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAldBMRgwFgYDVQQKEw9GNSBOZXR3b3JrcyBJbmMxHjAcBgNVBAsTFUNlcnRpZmljYXRlIEF1dGhvcml0eTEuMCwGA1UEAxMlRjUgSW50ZXJtZWRpYXRlIENlcnRpZmljYXRlIEF1dGhvcml0eTAeFw0xODEyMTMxOTU3NDlaFw0yODEyMTAxOTU3NDlaMIGnMQswCQYDVQQGEwJVUzETMBEGA1UECAwKV2FzaGluZ3RvbjEaMBgGA1UECgwRRjUgTmV0d29ya3MsIEluYy4xHjAcBgNVBAsMFUNlcnRpZmljYXRlIEF1dGhvcml0eTE1MDMGA1UEAwwsRjUgU1RHIElzc3VpbmcgQ2VydGlmaWNhdGUgQXV0aG9yaXR5IFRFRU0gVjExEDAOBgNVBAcMB1NlYXR0bGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC1zQVzFAkzJdDMk+blcmQ506+GhWZOoe32rSWpkztdH7nYHtE6ITx44nHErRLOXXcYsf8nQZyV2RiE5Gyxwvdg3ClC9XqfT702FxFWLPpD/I53cRZasYH3dFfdvDztEOlTsUrMfo7Bzfh7ZMaMkBczHno0DdP61lp6pfzsQRLSfdaWxLKkCxc3P2xDUyx9F7uX3lXsT042OuiZpoCrMumO53hmvw6ZtP6mH7d6dM7nhYhTIGxRMYrzEAHKl+JM0Jnaabwxw4UBMkxozxP+kLvDXrwLADjMslEuVeq1r7WwNa33y8aXfBUZpDCgJKfYvvPQIUD5d6ui0v7vwAQRgX/fAgMBAAGjZjBkMB0GA1UdDgQWBBSw3St90A/RXS561RcPmQJEFGssQjAfBgNVHSMEGDAWgBRz1uVFvQMN0SWZ8zjGfQLZ+vrt6TASBgNVHRMBAf8ECDAGAQH/AgEBMA4GA1UdDwEB/wQEAwIBhjANBgkqhkiG9w0BAQsFAAOCAgEABB8ygsfvpId2OPMh3jnTtEpfcJy80yu7vFVSMDQ/4xKTBSR0iFcCNmMJ8i4PL0E8RqFzcsUaG9Rq2uyiW71Y/+QiC0/xN8pXTua9zH1aYPLKTa62IB5Dnfax+QccNCehCAoJ/W4yVeY9/nHbSlYt8+eOMSdUJf/hcaPuHbLs6rJI9GHo9CNeBtWH0q+Xw3rRAXSrNXMg+CRE55JOVaDdzRUOEdf962Pd/MRN7+Sypyj2dR9rCJ/SKxf0HQr6NOGSAc3QbLun0bzew/0Nlww8UpCXV/ABiBFUBDvIhapMQqoErMuPm0CvqBdVCWafOa8qylHHOCkEUxTlxtk3WTwEI4RcrHnHVO4eIkstLe8+4HvKvCwXwoDlcms44lIzQpoPvVclkYYKH0d9GjY1dDSXxYeIm7aPeA6VutQoTd8ozKqZFjueESJB7JATC1q5PSiOhNIUr1d9Y7CbTpLWAl7ktJt5yZlcJBd3+5wztuCtwfQncjERRl8Sey3UuCjD836E7d4ZPldKUaJpDKpdzXIiiwDWCTL3G1iPBz+O1YyPAoQz6NFUsiHDnuIaGMfYLouf0ltuHTBwzcQbkFtH7PeY5Qwts617AQBy5lCJ3HLdJ9Hg3CwTXlBqFR+T/8vF0n6+AuE0ZFjmbJYJs0m4EObk0IOcex4ft33fPDEFWRJpHIs=",
                    "MIIGFzCCA/+gAwIBAgIBAjANBgkqhkiG9w0BAQsFADCBsDEYMBYGA1UEChMPRjUgTmV0d29ya3MgSW5jMSEwHwYDVQQLExhGNSBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkxHTAbBgkqhkiG9w0BCQEWDnJvb3RAbG9jYWxob3N0MRAwDgYDVQQHEwdTZWF0dGxlMQswCQYDVQQIEwJXQTELMAkGA1UEBhMCVVMxJjAkBgNVBAMTHUY1IFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5MB4XDTIyMDcyMTIxMTQxOVoXDTMyMDcxODIxMTQxOVowgYQxCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJXQTEYMBYGA1UEChMPRjUgTmV0d29ya3MgSW5jMR4wHAYDVQQLExVDZXJ0aWZpY2F0ZSBBdXRob3JpdHkxLjAsBgNVBAMTJUY1IEludGVybWVkaWF0ZSBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCg/uLiu6BbzIfZnehoaBepLVVQIeEgCUPxg6iHNANvlRC3FTElriLIDLJ3zAktdd3B1CSttG8sS4z3TKmN1+C1GXJRUZ2TlPRSaPBIhPs3xGFRgwBTJpTknY2oZITpfoDegAIuZcvfF5rwbcvndo2SiCbNHFytD/tIjWOJ+2a8k4T+8tBjaBxRygPSQycR6dciFo1ARJKfeWQV7Jkc0nzOd0T6JYh2Fjkyzx2EJca1zx88etIhfRg7XMo3SWLF6XJEoMBGoTeAAnE3oo+7KbBmcMYfcWGkwa/bOrBL4GCE/u3OS9z0wIoZ8/ExdIvwvXfYCrHO7Q7mW/TL9VbnXQjqQiUu6KUaw7SnP2VnqOmWxZyeKGMPnp1CDNzljo97NUq+YBXWNUMrdG/ahemcKoLQj6X9VNXrv5pE2u4HdsTHsXLE+bf4gvhWSPOoJR06d77C0eppMGmseYTIphvrFYbOkyUqJ3QPeh0alyRERPwZo7KWXbiWwwTs2Ya0IP4ndVxfnPJCAdyLs5dZcCPwaSZcqKS+ruGq/NCdpv9c4qQlog4cgPJaLjdvyhgttHxKFb8gLwensE2R5j2EKk/eDVSMZH7DMxAMVCOwAXC7yU//jzxbM79oLXJKtGUOqI5Lqo14oBQ9GN9jMadH7QIf98WUKxoI9jG2b7RVTaVI63xUuQIDAQABo2YwZDAdBgNVHQ4EFgQUc9blRb0DDdElmfM4xn0C2fr67ekwHwYDVR0jBBgwFoAUvt3/76pNZ1Iqkujy1aWYEmZs4bowEgYDVR0TAQH/BAgwBgEB/wIBATAOBgNVHQ8BAf8EBAMCAYYwDQYJKoZIhvcNAQELBQADggIBAF8EmEr06Legji041di2NbG42oQ0Jgaa4du/V9jloUp/N4Qo5t1upDrSQcEGdkLCgvGBDUKHaZWdJSRtoW4OxlNUfOeU0HEkt24TjwrW08eXDjmmDnqYjhPheeVJNMP2e0+Kj5l3ncTWPD/aS8HtZUdggpU8L9Y8vg6Tl143dZaePQEj+FHghmReIkRoJ2GT/hXFp17p0lTTlcjdRv/zU/Yvtp7F0JL8tjkMqy1Al5xZYDWZznamKdMUT71ikMFVHOVRgK4L+mfLGjA3rHT/hTWQ6EentQmWwv1+wG8fvShBL48YwIFCW02VxD/qdJjgcLGJ3KB9xxO2IBGo99bT2D1xLXgnu5odLWMIB3rUR2hKJWRJI+haODXgE8x+vzHjMpQjkv0Ud0TdL1/ULnLkW0rIiksRQvtWNXfJe33MnMx+P+cQ8wvpCbtcLZVVlnpqRmfiN+YK9stvZS0RKOi10WdR2tJpYOUi/tJ4dQ/u7erYUrmu/onVbe0M8P8w4CuDxhyEC3s1Gk4wppe9SqZgM2Op4FaRBzvm7oxMg27RngZ6BdK5JBlDY4SVXm5YxGkKMupTjXMo98pSn4mxlr5o4MU0YKsrUWENBM+MPUKb2rRRq0yyy0xsnvr33hr9OIlRjYYr06MiGHM5YLKxsJm73YivOhZKAwhWEHSA6uZ7dCvA",
                    "MIIGFzCCA/+gAwIBAgIBATANBgkqhkiG9w0BAQsFADCBsDEYMBYGA1UEChMPRjUgTmV0d29ya3MgSW5jMSEwHwYDVQQLExhGNSBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkxHTAbBgkqhkiG9w0BCQEWDnJvb3RAbG9jYWxob3N0MRAwDgYDVQQHEwdTZWF0dGxlMQswCQYDVQQIEwJXQTELMAkGA1UEBhMCVVMxJjAkBgNVBAMTHUY1IFJvb3QgQ2VydGlmaWNhdGUgQXV0aG9yaXR5MB4XDTE3MTAzMTIyMTQ1OVoXDTI3MTAyOTIyMTQ1OVowgYQxCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJXQTEYMBYGA1UEChMPRjUgTmV0d29ya3MgSW5jMR4wHAYDVQQLExVDZXJ0aWZpY2F0ZSBBdXRob3JpdHkxLjAsBgNVBAMTJUY1IEludGVybWVkaWF0ZSBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCg/uLiu6BbzIfZnehoaBepLVVQIeEgCUPxg6iHNANvlRC3FTElriLIDLJ3zAktdd3B1CSttG8sS4z3TKmN1+C1GXJRUZ2TlPRSaPBIhPs3xGFRgwBTJpTknY2oZITpfoDegAIuZcvfF5rwbcvndo2SiCbNHFytD/tIjWOJ+2a8k4T+8tBjaBxRygPSQycR6dciFo1ARJKfeWQV7Jkc0nzOd0T6JYh2Fjkyzx2EJca1zx88etIhfRg7XMo3SWLF6XJEoMBGoTeAAnE3oo+7KbBmcMYfcWGkwa/bOrBL4GCE/u3OS9z0wIoZ8/ExdIvwvXfYCrHO7Q7mW/TL9VbnXQjqQiUu6KUaw7SnP2VnqOmWxZyeKGMPnp1CDNzljo97NUq+YBXWNUMrdG/ahemcKoLQj6X9VNXrv5pE2u4HdsTHsXLE+bf4gvhWSPOoJR06d77C0eppMGmseYTIphvrFYbOkyUqJ3QPeh0alyRERPwZo7KWXbiWwwTs2Ya0IP4ndVxfnPJCAdyLs5dZcCPwaSZcqKS+ruGq/NCdpv9c4qQlog4cgPJaLjdvyhgttHxKFb8gLwensE2R5j2EKk/eDVSMZH7DMxAMVCOwAXC7yU//jzxbM79oLXJKtGUOqI5Lqo14oBQ9GN9jMadH7QIf98WUKxoI9jG2b7RVTaVI63xUuQIDAQABo2YwZDAdBgNVHQ4EFgQUc9blRb0DDdElmfM4xn0C2fr67ekwHwYDVR0jBBgwFoAUvt3/76pNZ1Iqkujy1aWYEmZs4bowEgYDVR0TAQH/BAgwBgEB/wIBATAOBgNVHQ8BAf8EBAMCAYYwDQYJKoZIhvcNAQELBQADggIBAGgXhdFaLvqYyzBTsc2jrfJWvnwwQztwkk++R2vR5Skwhy1ke5+fycmaiwERtOuqqjq0pJpFJiO61T0wlm/vF2HqsMMibvNgrSCvGurGyCdVTKahYNKqHWsevhhnqjoGWSlm7hgVz5wtGQoyImJMa3+qFvMtOZSFpHzSlteinLucPrA4EEuTNh1RjRNmq7J0oAl3+PG5bK5DpySOh4jX119G7P9VhX+aLVangYi9ZkBJgmx4tmsg7Caqg7RF0tIsnTdad9uI+WKty/vsXDntb8zzonTg59BhW3zMcT1p6Xutz4WyC0BHeculq+8LtLO0G2Dxxzeik/V9Z03mOW8bscjkPh5GcXtwTdSZiyh1ewGtyR0Jcj6vYqBLkXQtfX5JERuCuFcb15NE1Mr3V91kdJs1WPPY7fcwgPVEdBCa4Yo/FrwzoKuYqQIE8jnLEX+YOAcS8VS1eurPRl7v5ZZSMU2RnacvXL9TJ/Wk32KgUCOLjy2O3MmaPZLnasgDVQGXOdP4Q2pp7TRwjvR3GJvLCFQtvKBOZO35EhvF0AwAxi5PmTwSL3k3zdYlYADIyyo1YMhiS/FQueo06dtyShsoSPtmo7Jthus9xKxoyQVih11UdDieR9ZdikNRX805w1jc5O0DWFkq9AKDxLYKUkE/MxuvXzXls9RFHSwKMvzfxa0r"
                ],
                "use": "sig"
            }
        ]
    }
EOF

kubectl apply -f cpcl-non-prod.yaml

# 5. Scalable Function CNI Binary

# my NAD

cat <<EOF > my-macvlan.yaml  
apiVersion: "k8s.cni.cncf.io/v1"
kind: NetworkAttachmentDefinition
metadata:
  name: client-net
spec:
  config: '{
      "cniVersion": "0.3.1",
      "type": "macvlan",
      "master": "ens6",
      "mode": "bridge",
      "ipam": {}
    }'
---
apiVersion: "k8s.cni.cncf.io/v1"
kind: NetworkAttachmentDefinition
metadata:
  name: server-net
spec:
  config: '{
      "cniVersion": "0.3.1",
      "type": "macvlan",
      "master": "br0",
      "mode": "bridge",
      "ipam": {}
    }'
EOF

kubectl apply -f my-macvlan.yaml

cat <<EOF > orchestrator-values.yaml # added values for node selection
global:
  imagePullSecrets:
    - name: far-secret
image:
  repository: repo.f5.com/images
  pullPolicy: Always
nodeSelector:
  type: f5-control
EOF

helm install orchestrator oci://repo.f5.com/charts/orchestrator --values orchestrator-values.yaml --version v0.0.25-0.0.96

cat <<EOF > infrastructure-cr.yaml 
apiVersion: charts.k8s.f5net.com/v1alpha1
kind: SPKInfrastructure
metadata:
  name: bnk-dpu-infra
spec:
  networkAttachment:
  - name: default/client-net
  - name: default/server-net
  platformType: other
#  hugepages: true
  hugepages: false
#  sriovResources:
#    nvidia.com/bf3_p0_sf: "1"
#    nvidia.com/bf3_p1_sf: "1"
#  wholeClusterMode: "enabled"
  wholeClusterMode: "disabled"
  calicoRouter: "default"
  egress:
    json:
      ipPoolCidrInfo:
        cidrList:
        - name: vlan_cidr
          value: "192.168.20.0/24"
        - name: vlan_ipv6_cidr
          value: "2001::192:168:20:0/112"
        ipPoolList:
        - name: default-ipv4-ippool
          value: "10.244.0.0/16"
EOF

kubectl apply -f infrastructure-cr.yaml 


JWT="eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCIsImtpZCI6InYxIiwiamt1IjoiaHR0cHM6Ly9wcm9kdWN0LXRzdC5hcGlzLmY1bmV0d29ya3MubmV0L2VlL3YxL2tleXMvandrcyJ9.eyJzdWIiOiJUU1QtQTlBNjRCMTUtMEY0Ri00RUJFLThFNzUtQkU2Q0ZFQ0JFNTExIiwiaWF0IjoxNjM4MzgzODA0LCJpc3MiOiJGNSBJbmMuIiwiYXVkIjoidXJuOmY1OnRlZW0iLCJqdGkiOiJhMWYzOTY5MC01MmQ1LTExZWMtYmY4YS02ZDY3YmE2OWM1ZDkiLCJmNV9vcmRlcl90eXBlIjoicGFpZCJ9.jjgkl7YCK94sT6ubtZ6uqpF5razr6Ze43WMQbyc0jVzFzW-_jxeEDolRyPNu_VAK9Mier0OZRuaD6OJlmxyJob6pJwPg-jxgyeVEcGRoG8Mc1qPmiKzr2Jt-NRUCuiAX-ldwKKxXmRB2LQcPzSS0GywXPTO-ZpfBLzGq6I4xm9WP2HKXWT7KK5NQiG4yPwymfzV2i3YZMa_XbVVgQPG2GTs5MlRlyZ5M8BvqbHppENbHFjzfvq6Z9o6x0tAKMED_7Fdn-fioJNiweNTK1u_Go9kfUt-E5TyrEwK_0SrRH6TfHJ59JK7eQliHbMjDRd1ZbGjwvkNgAmnKnOMx2dUp9xaCnp1GbtgBTQGiJN3L-qgHIOoc0PIIH1FPemaJiXbCVAuPjQI4frTeT7Ay3M2JU4_51PeY8W5GFAdhVIZq9PR2RBIDw8-ioLIJ9jwko65DaO0fxOgINmdHTsPu0xHoHILd6_TBUJVzDUVLhL9C1SF2Y2LwrIYnUrBtmpqVw-N2d1gt-yelzM0d48AJ0rpWipXfOLLl4uRBIH4UFTex9ygdGNijFsmnIjBuj4njZs7uey5ES66Gbv6UyYtAs5lQV-cLZNIhozj_GZxsQytS4nnTqNoNoQM523BeQILF9DPSiooZmtebOLvrNzPNhpbzpzp-ALM6U-XYm5zUEMgRZjg"

cat <<EOF > instance-cr.yaml  
apiVersion: charts.k8s.f5net.com/v1alpha1
kind: SPKInstance
metadata:
  name: bnk-dpu
  namespace: default
spec:
  controller:
    watchNamespace: red,blue
  cwc:
    cpclConfig:
      jwt: ${JWT}
      operationMode: connected
  global:
    certmgr:
      issuerRef:
        group: cert-manager.io
        kind: ClusterIssuer
        name: bnk-ca-cluster-issuer
    imagePullSecrets:
    - name: far-secret
    imageRepository: repo.f5.com/images
    logging:
      fluentbitSidecar:
        fluentd:
          port: "54321"
  spkInfrastructure: bnk-dpu-infra
  spkManifest: unused
  afm:
    enabled: true
    pccd:
      enabled: true
      blob:
        maxFwBlobSizeMb: "512"
        maxNatBlobSizeMb: "512"
  tmm:
    replicaCount: 1
    nodeAssign:
      nodeSelector:
        app: f5-tmm
      tolerations:
        - key: "dpu"
          value: "true"
          operator: "Equal"
#    palCPUSet: "8-15"
#    usePhysMem: true
#    tmmMapresHugepages: 6144
    resources:
      limits:
        cpu: "1"
#        cpu: "8"
#        hugepages-2Mi: 13Gi 
        hugepages-2Mi: 2Gi
        memory: 2Gi
    debug:
      enabled: true
      resources:
        limits:
          cpu: 200m
          memory: 100Mi
        requests:
          cpu: 200m
          memory: 100Mi
#    xnetDPDKAllow:
#    - auxiliary:mlx5_core.sf.4,dv_flow_en=2
#    - auxiliary:mlx5_core.sf.5,dv_flow_en=2
    blobd:
      enabled: true
      resources:
        limits:
          cpu: "1"
          memory: "1Gi"
        requests:
          cpu: "1"
          memory: "1Gi"
    dynamicRouting:
      enabled: false
      configMapName: spk-bgp
    tmrouted:
      resources:
        limits:
          cpu: "300m"
          memory: "512Mi"
        requests:
          cpu: "300m"
          memory: "512Mi"
    tmmRouting:
      resources:
        limits:
          cpu: "700m"
          memory: "512Mi"
        requests:
          cpu: "700m"
          memory: "512Mi"
    sessiondb:
      useExternalStorage: "true"
EOF

kubectl apply -f instance-cr.yaml 
